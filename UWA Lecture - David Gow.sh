#!/bin/bash




basepath="http://prod.lcs.uwa.edu.au:8080"
unitdbpath="units.db"
cookiepath="/tmp/cooee-$USER-session-cookie.txt"
downloadhistoryfile="/dev/null"

# for os x
if [[ `uname` == Darwin ]]
then
	tac="tail -r"
else
	tac="tac -"
fi

function getLecture {
	lecpath=`wget -q -O - $1 | grep contentDir | sed s/.*contentDir=// | sed s/.streamDir=.*//`audio-vga.m4v
	if [[ "$lecpath" == "audio-vga.m4v" ]]; then
		echo "No link found for $2."
		return
	fi
	echo $2
	wget -c -nv -O "$2" $lecpath
	if [[ ! -s "$2" ]]; then
		echo "Failed to download $2 from $lecpath"
		rm $2
	fi
}

lectureIndex=1
startLecture=1
outputBase=lecture

function getNextLecture {
	if [ "$lectureIndex" -ge "$startLecture" ]
	then
		if grep --fixed-strings --no-messages --silent $1 $downloadhistoryfile
		then
			echo Skipping lecture $lectureIndex, already downloaded
		else
			getLecture $1 "$outputBase$lectureIndex.m4v"
			echo $1 >> $downloadhistoryfile
		fi
	else
		echo Skipping lecture $lectureIndex
	fi
	let lectureIndex=lectureIndex+1
}

function getUnit {
	lecindexpath=$basepath`wget -q -O - $1 | grep "ecp/section" | sed s/.*src=.// | sed s/..scrolling.*//`
	sectionid=`echo $lecindexpath | sed s:.*section/:: | sed s/\?.*//`
	echo "Got unit section id $sectionid"
	echo "Getting session cookie..."
	wget --keep-session-cookies --save-cookies "$cookiepath" -q -O /dev/null "$lecindexpath"

	jsonlecindexpath="$basepath/ecp/api/$sectionid/section-data.json?skipCache=false&pageIndex=1&pageSize=256&callback=loadSectionData"
	refererpath="$basepath/ecp/html/echo_center.html?sectionId=$sectionid"
	lectures=`wget --load-cookies "$cookiepath" --referer="$refererpath" -q -O - "$jsonlecindexpath" | grep -o '"richMedia":"[^"]\+' | sed s/.*:\"// | sed s/\",// | $tac`



	for i in $lectures; do
		getNextLecture $i
	done
}

function showHelp {
	echo "I'm hungry, please pass me a unit or lecture URL."
	echo
	echo "It should look like:"
	echo "   cooee.sh -l http://prod.lcs.uwa.edu.au:8080/ess/echo/presentation/<guid>"
	echo "   cooee.sh -u http://prod.lcs.uwa.edu.au:8080/ess/portal/section/<number>"
	echo
	echo "You should be able to find the URL on your unit webpage."
	echo
	echo "Lectures will be saved as [basename][n].m4v in the current directory."
	echo
	echo "Options:"
	echo " -o [basename]    set the base name for lectures (default: 'lecture')"
	echo " -s [start]       start at lecture n."
	echo " -z               save download history so lectures can be renamed (use with -u)"
	echo " -l [lectureUrl]  download a single lecture to [basename].m4v"
	echo " -u [unitUrl]     download all lectures for the unit at URL"
	echo
	echo "Due to lazy coding, -u or -l must be the last option on a line."
	exit 1
}

echo "Cooee: An echo360 lecture ripper."
echo "Version 1.1: Now with Echo360 2013 support!"

while getopts "o:s:zl:u:" opt
do
	case $opt in
	o)
		outputBase=$OPTARG
		;;
	s)
		startLecture=$OPTARG
		;;
	z)
		downloadhistoryfile=".cooee-downloaded"
		;;
	l)
		getLecture $OPTARG "$outputBase.m4v"
		exit 0
		;;
	u)
		getUnit $OPTARG
		exit 0
		;;
	esac
done

showHelp

