# 
# This code was provided to me and is unlicened.
# 
# All code written by Dylan Reginald Johnson (forgive the typos)
# 
# 

lectureID='12511657-4927-4be5-87e7-c70b455098a4'
portalURL='http://prod.lcs.uwa.edu.au:8080'
fetchExt='/ess/portal/section/'
jsonStuff='skipCache=false&pageIndex=1&pageSize=100&callback=loadSectionData'
fileExt='audio-vga.m4v'

portalExt=$(curl -v ${portalURL}${fetchExt}${lectureID} | grep frameborder | sed s#.*src=\"## | sed s#\&moderator.*##)

#echo ${portalURL}${portalExt}

cookies=$(curl -v ${portalURL}${portalExt} 2>&1 | grep Set-Cookie | sed s#.*JSESSIONID=## | sed s#\;.*##)

echo 'OM NOM NOM COOKIE IS '${cookies}

data=$(curl --cookie "JSESSIONID="${cookies} -v ${portalURL}"/ecp/api/"${lectureID}"/section-data.json?"${jsonStuff} | sed s#\,#\\n#g | grep thumb | sed s#.*http#http# | sed s#\/synopsis.*#\,#)

echo ${data}

IFS=','

for i in "${data[@]}"
do
   echo $i'derp'
done