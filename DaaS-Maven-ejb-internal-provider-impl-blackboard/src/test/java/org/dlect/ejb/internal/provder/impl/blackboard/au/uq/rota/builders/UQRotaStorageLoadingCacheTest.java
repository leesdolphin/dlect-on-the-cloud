/*
 * This file is part of DLect. DLect is a suite of code that facilitates the downloading of lecture recordings.
 *
 * Copyright © 2014 Lee Symes.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.builders;

import com.google.common.base.Optional;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import java.net.URL;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQCampus;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQRotaOffering;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQRotaSemester;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQRotaSubject;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQRotaSubjectSemesterPair;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQStream;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQStreamEventData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author lee
 */
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("unchecked")
public class UQRotaStorageLoadingCacheTest {

    @Mock
    private LoadingCache<Integer, UQRotaSemester> semesters;

    @Mock
    private LoadingCache<String, UQRotaSubject> subjects;

    @Mock
    private LoadingCache<UQRotaSubjectSemesterPair, Optional<UQRotaOffering>> offerings;

    @Mock
    private LoadingCache<String, Map<String, URL>> lectureData;

    @Mock
    private LoadingCache<UQRotaSubjectSemesterPair, Multimap<UQStream, UQStreamEventData>> streamData;

    @InjectMocks
    private UQRotaStorage testObject;

    @Test
    public void testGetSemester() throws Exception {
        UQRotaSemester sem = new UQRotaSemester();
        sem.setName("dlhgk");
        sem.setEndWeek(15);
        sem.setStartWeek(1);
        sem.setYear(2014);

        when(semesters.get(1)).thenReturn(sem);

        UQRotaSemester gotSem = testObject.getSemester(1);

        verify(semesters).get(1);

        assertEquals("Returned a different semester", sem, gotSem);
    }

    @Test
    public void testGetSemesterThrowsException() throws Exception {
        when(semesters.get(1)).thenThrow(new ExecutionException(null));

        try {
            testObject.getSemester(1);
            fail("Y U NO EXCEPTION? :?");
        } catch (ExecutionException ex) {
            verify(semesters).get(1);
        }
    }

    @Test
    public void testGetSubject() throws Exception {
        String key = "KEY";
        UQRotaSubject sub = new UQRotaSubject();
        sub.setName("dlhgk");
        sub.setCode("Code");
        sub.setOfferings(null);

        when(subjects.get(key)).thenReturn(sub);

        UQRotaSubject gotSub = testObject.getSubject(key);

        verify(subjects).get(key);

        assertEquals("Returned a different semester", sub, gotSub);
    }

    @Test
    public void testGetSubjectThrowsException() throws Exception {
        String key = "KEY";
        when(subjects.get(key)).thenThrow(new ExecutionException(null));

        try {
            testObject.getSubject(key);
            fail("Y U NO EXCEPTION? :?");
        } catch (ExecutionException ex) {
            verify(subjects).get(key);
        }
    }

    @Test
    public void testGetOffering() throws Exception {
        int sem = 1;
        String sub = "KEY";

        UQRotaSubjectSemesterPair key = new UQRotaSubjectSemesterPair(sub, sem);
        UQRotaOffering offer = new UQRotaOffering();
        offer.setId(100);
        offer.setSemId(1);
        offer.setSeries(null);

        when(offerings.get(key)).thenReturn(Optional.of(offer));

        UQRotaOffering gotSub = testObject.getOffering(sub, sem);

        verify(offerings).get(key);

        assertEquals("Returned a different semester", offer, gotSub);
    }

    @Test
    public void testGetOfferingWithNoResponce() throws Exception {
        int sem = 1;
        String sub = "KEY";

        UQRotaSubjectSemesterPair key = new UQRotaSubjectSemesterPair(sub, sem);

        when(offerings.get(key)).thenReturn(Optional.<UQRotaOffering>absent());

        UQRotaOffering gotOffer = testObject.getOffering(sub, sem);

        verify(offerings).get(key);

        assertNull("Returned a semester when it was indicated as absent", gotOffer);
    }

    @Test
    public void testGetOfferingThrowsException() throws Exception {
        int sem = 1;
        String sub = "KEY";

        UQRotaSubjectSemesterPair key = new UQRotaSubjectSemesterPair(sub, sem);

        when(offerings.get(key)).thenThrow(new ExecutionException(null));

        try {
            testObject.getOffering(sub, sem);
            fail("Y U NO EXCEPTION? :?");
        } catch (ExecutionException ex) {
            verify(offerings).get(key);
        }
    }

    @Test
    public void testGetStreamData() throws Exception {
        int sem = 1;
        String sub = "KEY";

        UQRotaSubjectSemesterPair key = new UQRotaSubjectSemesterPair(sub, sem);
        Multimap<UQStream, UQStreamEventData> streams = ImmutableMultimap.of(new UQStream("SSDFSDF1"), new UQStreamEventData(UQCampus.GATTON, "Bdg1", "Rm102", new Date()));

        when(streamData.get(key)).thenReturn(streams);

        Multimap<UQStream, UQStreamEventData> gotStreams = testObject.getStreamData(sub, sem);

        verify(streamData).get(key);

        assertEquals("Different stream data was returned", streams, gotStreams);
    }

    @Test
    public void testGetStreamDataThrowsException() throws Exception {
        int sem = 1;
        String sub = "KEY";

        UQRotaSubjectSemesterPair key = new UQRotaSubjectSemesterPair(sub, sem);

        when(streamData.get(key)).thenThrow(new ExecutionException(null));

        try {
            testObject.getStreamData(sub, sem);
            fail("Y U NO EXCEPTION? :?");
        } catch (ExecutionException ex) {
            verify(streamData).get(key);
        }
    }

    @Test
    public void testGetLectureData() throws Exception {

        String key = "KEY";
        ImmutableMap<String, URL> lecData = ImmutableMap.of("ROW KEY", new URL("http://www.google.com/SGSDFGSDFGSDFGSDFGSDFGSDFG"));

        when(lectureData.get(key)).thenReturn(lecData);

        Map<String, URL> gotLecData = testObject.getBaseLectureUrl(key);

        verify(lectureData).get(key);

        assertEquals("Different stream data was returned", lecData, gotLecData);
    }

    @Test
    public void testGetLectureDataThrowsException() throws Exception {
        String key = "KEY";

        when(lectureData.get(key)).thenThrow(new ExecutionException(null));

        try {
            testObject.getBaseLectureUrl(key);
            fail("Y U NO EXCEPTION? :?");
        } catch (ExecutionException ex) {
            verify(lectureData).get(key);
        }
    }

}
