/*
 * This file is part of DLect. DLect is a suite of code that facilitates the downloading of lecture recordings.
 *
 * Copyright © 2014 Lee Symes.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.builders;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;
import javax.enterprise.inject.Instance;
import org.apache.http.client.methods.HttpUriRequest;
import org.dlect.internal.beans.HttpConfigurationBean;
import org.dlect.log.Customisers;
import org.dlect.test.helper.LoggingSetup.LoggingSetupReset;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.dlect.test.helper.DataLoader.loadResource;
import static org.dlect.test.helper.LoggingSetup.disableLogging;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author lee
 */
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("unchecked")
@Ignore
public class UQBlackboardLectureDataBuilderTest {

    public static final String SINGLE_LECTURE_RESOURCE_LOCATION = "org/dlect/ejb/internal/provder/impl/blackboard/uq/rota/builders/LectureDataBuilder-SingleLecture.txt";
    public static final String INVALID_LECTURE_RESOURCE_LOCATION = "org/dlect/ejb/internal/provder/impl/blackboard/uq/rota/builders/LectureDataBuilder-BadLectureUrl.txt";
    public static final String DOUBLE_LECTURE_RESOURCE_LOCATION = "org/dlect/ejb/internal/provder/impl/blackboard/uq/rota/builders/LectureDataBuilder-TwoLecturesInFullPage.html";

    @Captor
    private ArgumentCaptor<HttpUriRequest> req;

    @Mock
    private Instance<HttpConfigurationBean> httpBeanInstance;

    @Mock
    private HttpConfigurationBean httpBean;

//    @InjectMocks
//    private UQBlackboardLectureDataBuilder testObject;

    private LoggingSetupReset disableLogging;

    @Before
    public void initHttpConfigInstance() {
        when(httpBeanInstance.get()).thenReturn(httpBean);
        disableLogging = disableLogging(Customisers.LOG);
    }

    @After
    public void resetLoggingLevels() {
        disableLogging.reset();
    }

    @Test
    public void testLoadIOException() throws Exception {
        String url = "http://www.google.com/Dont-Access-Me";

        URI uri = new URL(url).toURI();

        when(httpBean.getAsString(any(HttpUriRequest.class))).thenThrow(new IOException());
//
//        try {
//            testObject.load(url);
//            fail("Where's the thrown exception?");
//        } catch (IOException e) {
//            verify(httpBeanInstance).get();
//
//            verify(httpBean).getAsString(req.capture());
//            assertEquals(req.getValue().getURI(), uri);
//            assertEquals(req.getValue().getMethod(), "GET");
//        }
    }

    @Test
    public void testLoadNoMatches() throws Exception {
        String url = "http://www.google.com/Dont-Access-Me";
        String result = "Oops, no Lectures here, I don't know what happened.";
//
//        // To be sure, this is a sortof test for the Regexp.
//        assertFalse(UQBlackboardLectureDataBuilder.LECTURE_DATA_REGEXP.matcher(result).find());
//
//        URI uri = new URL(url).toURI();
//
//        when(httpBean.getAsString(any(HttpUriRequest.class))).thenReturn(result);
//
//        Map<String, URL> load = testObject.load(url);
//
//        verify(httpBeanInstance).get();
//
//        verify(httpBean).getAsString(req.capture());
//        assertEquals(req.getValue().getURI(), uri);
//        assertEquals(req.getValue().getMethod(), "GET");
//
//        assertTrue("Load is not empty: " + load.toString(), load.isEmpty());
    }

    @Test
    public void testLoadSingleMatch() throws Exception {
        String url = "http://www.google.com/Dont-Access-Me";
        String result = loadResource(SINGLE_LECTURE_RESOURCE_LOCATION);
//
//        assertTrue(UQBlackboardLectureDataBuilder.LECTURE_DATA_REGEXP.matcher(result).find());
//
//        String bbid = "_410796_1";
//        String baseUrl = "https://learn.uq.edu.au/webapps/any-acrbb-BBLEARN//seamless?id=_410796_1&url=http%3A%2F%2Flecture.recordings.uq"
//                         + ".edu.au%3A8080%2Fess%2Fecho%2Fpresentation%2Fb5628c73-cfb9-46c1-ae82-4c503434bf10";
//
//        when(httpBean.getAsString(any(HttpUriRequest.class))).thenReturn(result);
//
//        Map<String, URL> load = testObject.load(url);
//
//        verify(httpBeanInstance).get();
//
//        verify(httpBean).getAsString(req.capture());
//        assertEquals(req.getValue().getURI(), new URL(url).toURI());
//        assertEquals(req.getValue().getMethod(), "GET");
//
//        for (Entry<String, URL> cell : load.entrySet()) {
//            assertEquals("Blackboard ID was plucked out of thin air - just like this test :D", bbid, cell.getKey());
//
//            checkUrl(cell, baseUrl);
//        }
    }

    @Test
    public void testLoadInvalidMatch() throws Exception {

        String url = "http://www.google.com/Dont-Access-Me";
        String result = loadResource(INVALID_LECTURE_RESOURCE_LOCATION);
//
//        assertTrue(UQBlackboardLectureDataBuilder.LECTURE_DATA_REGEXP.matcher(result).find());
//
//        when(httpBean.getAsString(any(HttpUriRequest.class))).thenReturn(result);
//
//        Map<String, URL> load = testObject.load(url);
//
//        verify(httpBeanInstance).get();
//
//        verify(httpBean).getAsString(req.capture());
//        assertEquals(req.getValue().getURI(), new URL(url).toURI());
//        assertEquals(req.getValue().getMethod(), "GET");
//
//        assertTrue("Load is not empty. This should silently ignore invalid urls: " + load.toString(), load.isEmpty());
    }

    @Test
    public void testLoadTwoMatches() throws Exception {
        String url = "http://www.google.com/Dont-Access-Me";
        String result = loadResource(DOUBLE_LECTURE_RESOURCE_LOCATION);
//
//        assertTrue(UQBlackboardLectureDataBuilder.LECTURE_DATA_REGEXP.matcher(result).find());
//
//        String bbid1 = "_391393_1";
//        String baseUrl1 = "https://learn.uq.edu.au/webapps/any-acrbb-BBLEARN//seamless?id=_391393_1&url=http%3A%2F%2Flecture.recordings.uq"
//                          + ".edu.au%3A8080%2Fess%2Fecho%2Fpresentation%2F6cdad7a2-1db0-4a07-a12e-8e994bd5116e";
//        String bbid2 = "_397346_1";
//        String baseUrl2 = "https://learn.uq.edu.au/webapps/any-acrbb-BBLEARN//seamless?id=_397346_1&url=http%3A%2F%2Flecture.recordings.uq"
//                          + ".edu.au%3A8080%2Fess%2Fecho%2Fpresentation%2Fac1a7b3d-eb16-41bc-9ea9-6439eda92c9f";
//
//        when(httpBean.getAsString(any(HttpUriRequest.class))).thenReturn(result);
//
//        Map<String, URL> load = testObject.load(url);
//
//        verify(httpBeanInstance).get();
//
//        verify(httpBean).getAsString(req.capture());
//        assertEquals(req.getValue().getURI(), new URL(url).toURI());
//        assertEquals(req.getValue().getMethod(), "GET");
//
//        for (Entry<String, URL> cell : load.entrySet()) {
//            if (cell.getKey().equals(bbid1)) {
//                checkUrl(cell, baseUrl1);
//            } else if (cell.getKey().equals(bbid2)) {
//                checkUrl(cell, baseUrl2);
//            }
//
//        }
    }

    public void checkUrl(Map.Entry<String, URL> cell, String baseUrl) {
        String genUrl = cell.getValue().toString();
        assertTrue("Generated URL(\"" + genUrl + "\") doesn't start with the correct value.", genUrl.startsWith(baseUrl));
    }

}
