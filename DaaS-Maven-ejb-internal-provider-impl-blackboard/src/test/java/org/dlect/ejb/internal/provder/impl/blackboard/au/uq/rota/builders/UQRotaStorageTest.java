/*
 * This file is part of DLect. DLect is a suite of code that facilitates the downloading of lecture recordings.
 *
 * Copyright © 2014 Lee Symes.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.builders;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import java.net.URL;
import java.util.Date;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQCampus;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQRotaOffering;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQRotaSubjectSemesterPair;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQStream;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQStreamEventData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 *
 * @author lee
 */
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("unchecked")
public class UQRotaStorageTest {

    @Mock
    private UQRotaSemesterBuilder semB;
    @Mock
    private UQRotaSubjectBuilder subB;
    @Mock
    private UQRotaOfferingBuilder offerB;
    @Mock
    private UQRotaStreamDataBuilder streamDataB;

    @InjectMocks
    private UQRotaStorage testObject;

    @Test
    public void testInit() throws Exception {

        int sem = 1;
        String sub = "KEY";

        UQRotaSubjectSemesterPair key = new UQRotaSubjectSemesterPair(sub, sem);

        when(semB.load(1)).thenAnswer(RETURNS_MOCKS);
        when(subB.load("KEY")).thenAnswer(RETURNS_MOCKS);
        when(offerB.load(key)).thenReturn(Optional.<UQRotaOffering>absent());
        when(streamDataB.load(key)).thenReturn(ImmutableMultimap.of(new UQStream("dlkfjg"), new UQStreamEventData(UQCampus.GATTON, "sdfg", "dfg", new Date())));

        testObject.init();

        testObject.getSemester(1);
        verify(semB).load(1);

        testObject.getSubject("KEY");
        verify(subB).load("KEY");

        testObject.getOffering(sub, sem);
        verify(offerB).load(key);

        testObject.getStreamData(sub, sem);
        verify(streamDataB).load(key);
    }

}
