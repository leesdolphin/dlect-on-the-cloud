/*
 * This file is part of DLect. DLect is a suite of code that facilitates the downloading of lecture recordings.
 *
 * Copyright © 2014 Lee Symes.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.builders;

import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.builders.UQRotaOfferingBuilder;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.builders.UQRotaStorage;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import java.net.URI;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import javax.enterprise.inject.Instance;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQRotaOffering;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQRotaSubject;
import org.dlect.ejb.internal.provder.impl.blackboard.au.uq.rota.objects.UQRotaSubjectSemesterPair;
import org.dlect.helpers.JAXBHelper;
import org.dlect.helpers.JAXBHelper.JaxbBindingSet;
import org.dlect.log.Customisers;
import org.dlect.test.helper.LoggingSetup.LoggingSetupReset;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.dlect.test.helper.LoggingSetup.disableLogging;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author lee
 */
@RunWith(PowerMockRunner.class)
@SuppressWarnings("unchecked")
@PrepareForTest({JaxbBindingSet.class, JAXBHelper.class})
public class UQRotaOfferingBuilderTest {

    private static final int TEST_OFFERING_ID = 1021;
    private static final int TEST_SEMESTER = 2014;
    private static final UQRotaSubjectSemesterPair subSemPair = new UQRotaSubjectSemesterPair("KEY", TEST_SEMESTER);

    @Captor
    private ArgumentCaptor<URI> uriCaptor;

    @Mock
    private Instance<UQRotaStorage> instance;

    @Mock
    private UQRotaStorage storage;
    @Mock
    private UQRotaSubject subject;

    @InjectMocks
    private UQRotaOfferingBuilder testObject;

    private LoggingSetupReset disableLogging;

    @Before
    public void initHttpConfigInstance() throws ExecutionException {
        disableLogging = disableLogging(Customisers.LOG);

        PowerMockito.mockStatic(JAXBHelper.class);

        when(instance.get()).thenReturn(storage);
    }

    @After
    public void resetLoggingLevels() {
        disableLogging.reset();
    }

    @Test(expected = ExecutionException.class)
    public void testLoadSubjectFail() throws Exception {
        when(storage.getSubject(anyString())).thenThrow(ExecutionException.class);

        testObject.load(subSemPair);
    }

    @Test
    public void testLoadNoOffering() throws Exception {
        when(storage.getSubject(anyString())).thenReturn(subject);
        when(subject.getOfferings()).thenReturn(new ArrayList<UQRotaOffering>());

        Optional<UQRotaOffering> ret = testObject.load(subSemPair);

        assertFalse(ret.isPresent());

        verify(subject).getOfferings();
    }

    @Test
    public void testLoadOffering() throws Exception {
        UQRotaOffering subOffer = mock(UQRotaOffering.class);
        when(subOffer.getId()).thenReturn(TEST_OFFERING_ID);
        when(subOffer.getSemId()).thenReturn(TEST_SEMESTER);

        UQRotaOffering jaxbOffer = mock(UQRotaOffering.class);

        when(storage.getSubject(anyString())).thenReturn(subject);
        when(subject.getOfferings()).thenReturn(Lists.newArrayList(subOffer));

        PowerMockito.when(JAXBHelper.unmarshalFromUri((URI) any(), (JaxbBindingSet) any())).thenReturn(jaxbOffer);

        Optional<UQRotaOffering> ret = testObject.load(subSemPair);

        assertTrue(ret.isPresent());
        assertEquals(jaxbOffer, ret.get());

        // Verify method calls.
        verify(subject).getOfferings();

        // Verify JAXB calls
        PowerMockito.verifyStatic();
        JAXBHelper.unmarshalFromUri(uriCaptor.capture(), any(JaxbBindingSet.class));

        URI u = uriCaptor.getValue();

        assertTrue(u.toString().contains("rota.eait.uq.edu.au"));
        assertTrue(u.toString().endsWith("/offering/" + TEST_OFFERING_ID + ".xml"));
    }

    @Test
    public void testLoadMultiOffering() throws Exception {
        UQRotaOffering subOffer1 = mock(UQRotaOffering.class);
        when(subOffer1.getId()).thenReturn(TEST_OFFERING_ID);
        when(subOffer1.getSemId()).thenReturn(TEST_SEMESTER);
        
        UQRotaOffering subOffer2 = mock(UQRotaOffering.class);
        when(subOffer2.getId()).thenReturn(TEST_OFFERING_ID + 1);
        when(subOffer2.getSemId()).thenReturn(TEST_SEMESTER + 1);

        UQRotaOffering jaxbOffer = mock(UQRotaOffering.class);

        when(storage.getSubject(anyString())).thenReturn(subject);
        when(subject.getOfferings()).thenReturn(Lists.newArrayList(subOffer1, subOffer2));

        PowerMockito.when(JAXBHelper.unmarshalFromUri((URI) any(), (JaxbBindingSet) any())).thenReturn(jaxbOffer);

        Optional<UQRotaOffering> ret = testObject.load(subSemPair);

        assertTrue(ret.isPresent());
        assertEquals(jaxbOffer, ret.get());

        // Verify method calls.
        verify(subject).getOfferings();

        // Verify JAXB calls
        PowerMockito.verifyStatic();
        JAXBHelper.unmarshalFromUri(uriCaptor.capture(), any(JaxbBindingSet.class));

        URI u = uriCaptor.getValue();

        assertTrue(u.toString().contains("rota.eait.uq.edu.au"));
        assertTrue(u.toString().endsWith("/offering/" + TEST_OFFERING_ID + ".xml"));
    }

}
