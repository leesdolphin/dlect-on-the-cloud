/*
 * This file is part of DLect. DLect is a suite of code that facilitates the downloading of lecture recordings.
 * 
 * Copyright © 2014 Lee Symes.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package org.dlect.ejb.internal.provder.impl.blackboard.au.rmit;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import org.dlect.ejb.internal.provder.impl.blackboard.BlackboardUniversityCustomisation;
import org.dlect.ejb.internal.provder.lecture.impl.BlackboardCourseMapItem;
import org.dlect.export.Lecture;
import org.dlect.export.Semester;
import org.dlect.export.Subject;
import org.dlect.internal.data.merge.PartialLectureContent;
import org.dlect.internal.data.merge.PartialLectureWithStream;
import org.dlect.internal.data.merge.PartialSemester;
import org.dlect.internal.data.merge.PartialStream;
import org.dlect.internal.data.merge.PartialSubjectWithSemester;

/**
 *
 * @author lee
 */
@Stateless
public class RMITBlackboardUniversityCustomisation implements BlackboardUniversityCustomisation {

    private static final String UQ_LECTOPIA_LINK_TYPE = "resource/x-apreso";

    @Override
    public List<PartialStream> getAllStreamData(Semester semester, Subject subject) {
        PartialStream ps = new PartialStream();
        ps.setName("Unknown");
        return Lists.newArrayList(ps);
    }

    @Override
    public Set<PartialLectureContent> getDownloadsFor(Semester semester, Subject subject, Lecture l) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Optional<PartialLectureWithStream> getLectureDataFor(Semester semester, Subject subject, URL rootUrl,
                                                                BlackboardCourseMapItem item) {
        if (UQ_LECTOPIA_LINK_TYPE.equals(item.getLinktype())) {
            return Optional.absent();
        }
        return Optional.absent();
    }

    @Override
    public Optional<PartialSemester> getSemesterFor(String semCode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Optional<PartialStream> getStreamDataFor(Semester semester, Subject subject, String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Optional<PartialSubjectWithSemester> getSubjectDataFor(String bbid, String name, String courseid, Date enrollmentdate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<String> getUniversityCode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
