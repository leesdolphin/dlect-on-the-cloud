/*
 * This file is part of DLect. DLect is a suite of code that facilitates the downloading of lecture recordings.
 *
 * Copyright © 2014 Lee Symes.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dlect.ejb.internal.provder.impl.blackboard;

import java.io.Serializable;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import org.dlect.except.DLectException;
import org.dlect.export.University;
import org.dlect.helpers.DataHelpers;
import org.dlect.object.ResultType;

import static org.dlect.except.DLectExceptionBuilder.build;

/**
 *
 * @author lee
 */
@Startup
@Stateless
public class BlackboardUniversityCustomisationProvider implements Serializable {

    @Inject
    Instance<BlackboardUniversityCustomisation> customisers;

    public BlackboardUniversityCustomisation getCustomiserFor(University u) throws DLectException {
        for (BlackboardUniversityCustomisation buc : customisers) {
            if (DataHelpers.copyReplaceNull(buc.getUniversityCode()).contains(u.getCode())) {
                return buc;
            }
        }

        throw build(ResultType.NOT_SUPPORTED, "No customiser for the university");
    }

}
