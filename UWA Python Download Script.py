#!/usr/bin/python

# 
# This code was provided to me and is unlicened.
# 
# All code written by Brett Drawing (forgive the typos)
# 

import json
import requests
import datetime
from StringIO import StringIO
from lxml import etree

portalURL='http://prod.lcs.uwa.edu.au:8080'
fetchExt='/ess/portal/section/'
jsonSettings='skipCache=false&pageIndex=1&pageSize=100&callback=loadSectionData'

lectureIDfile='/home/serviledunce/podcaster/lectureID.txt'

with open(lectureIDfile) as f:
#    lectureIDs = f.readlines()
    for line in f:
        lectureID = line.rstrip('\n')
        portalExtUrl = portalURL + fetchExt + lectureID
        print 'portal extension URL = ',
        print portalExtUrl
        portalExtResponse = requests.get(portalExtUrl)

#        root = etree.HTML(response.text)
        parser = etree.HTMLParser()
        tree   = etree.parse(StringIO(portalExtResponse.text), parser)
#        print tree.docinfo.public_id
        root = tree.getroot()
#        print etree.tostring(root, pretty_print=True, method="html")
        dociframe = root.findall(".//iframe")[0]
#        print dociframe.tag
        portalExtFull = dociframe.get('src')
        portalExt = portalExtFull.rsplit('&',1)[0]
        print 'portal extension = ' + portalExt

        cookieResponse = requests.get(portalURL + portalExt)
        print 'cookie = ' + cookieResponse.cookies['JSESSIONID']

        captureSummaryResponse = requests.get(portalURL + "/ecp/api/" + lectureID + "/section-data.json?" + jsonSettings, cookies=cookieResponse.cookies)
        captureSummaryJson = captureSummaryResponse.text.split('(',1)[1].rstrip(');')
        captureSummary = json.loads(captureSummaryJson)
        #print json.dumps(captureSummary, indent=4, separators=(',', ': '))
        #the final JSON response has bee parsed, now we get to make sense of the data and build our podcast

        #   echo -e "<?xml version=\"1.0\"?>\n<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">" >> $feedName
        #set up a new XML tree
        #NSMAP = {atom : 'http://www.w3.org/2005/Atom'}
        #nsmap=NSMAP
        ATOM_NS='http://www.w3.org/2005/Atom'
        ATOM = "{%s}" % ATOM_NS
        MEDIA_NS='http://www.idfk.com/media'
        MEDIA= "{%s}" % MEDIA_NS
        NSMAP = {'atom':ATOM_NS, 'media':MEDIA_NS}

        root = etree.Element('rss', version='2.0', nsmap=NSMAP)
        tree = etree.ElementTree(root)

        chan = etree.SubElement(root, 'channel')

        #start scraping data from the JSON
        #print 'course name = ' + captureSummary['section']['course']['name']

        etree.SubElement(chan, ATOM + 'link', href='http://ServileDunce.com/' + captureSummary['section']['course']['identifier'] + 'audio.xml', rel="self", type="application/rss+xml")
        etree.SubElement(chan, 'title').text = captureSummary['section']['course']['name']
        etree.SubElement(chan, 'link').text = portalURL + fetchExt + lectureID
        etree.SubElement(chan, 'description').text = 'A podcast for Echo Lectures from ' + captureSummary['section']['course']['identifier']
        etree.SubElement(chan, 'language').text = 'en-au'
        #2013-08-14T09:58:00.000+08:00
        pubDate = datetime.datetime.now().strftime("%a, %d %b %Y %H:%M:%S ") + '+8000'
        etree.SubElement(chan, 'pubDate').text = pubDate 
        etree.SubElement(chan, 'lastBuildDate').text = pubDate
        etree.SubElement(chan, 'docs').text = 'http://blogs.law.harvard.edu/tech/rss'

        #sectionRoles = captureSummary['section']['sectionRoles']
        #print 'presenters are :'
        #for item in sectionRoles:
        #    print '    ' + item['person']['firstName'] + " " + item['person']['lastName']

        presentations = captureSummary['section']['presentations']['pageContents']

        presPage = int(captureSummary['section']['presentations']['pageIndex'])

        presPerPage = int(captureSummary['section']['presentations']['pageSize'])

        presQty = int(captureSummary['section']['presentations']['totalResults'])
        presListStart = 1 + ((presPage - 1) * presPerPage)
        presListEnd = min(presQty, (presPage * presPerPage))

        #print "presentations %d to %d :" % (presListStart, presListEnd)
        for item in presentations:

            pres = etree.SubElement(chan, 'item')
            
            datevalue = datetime.datetime.strptime(item['startTime'][:-6], '%Y-%m-%dT%H:%M:%S.%f')

            #<title>PHYS3302 week 39 Thu, 26 Sep 2013 12:58:00 +0800</title>
            #print '    title = ' + item['title']
            etree.SubElement(pres, 'title').text = captureSummary['section']['course']['identifier'] +  datevalue.strftime(" %a %d%b %H:%M")  #item['title']
            
            #<description>a lecture from PHYS3302 week 39 Thu, 26 Sep 2013 12:58:00 +0800</description>
            #print '    week = ' + item['week']
            etree.SubElement(pres, 'description').text = 'a lecture from ' + captureSummary['section']['course']['identifier'] + ' week ' + item['week']

            #<pubDate>Thu, 26 Sep 2013 12:58:00 +0800</pubDate>

            etree.SubElement(pres, 'pubDate').text = datevalue.strftime("%a, %d %b %Y %H:%M:%S ") + item['startTime'][-6:]

            #<link>http://echo360-test.its.uwa.edu.au/echocontent/1339/4/ff6ed436-a3d3-4023-9ff0-86bde10de2ee/audio.mp3</link>
            mediaRoot =  item['thumbnails'][0].split('synopsis')[0]
            #print '    media root = ' + mediaRoot
            etree.SubElement(pres, 'link').text = mediaRoot + 'audio.mp3'
            
            #<enclosure url="http://echo360-test.its.uwa.edu.au/echocontent/1339/4/ff6ed436-a3d3-4023-9ff0-86bde10de2ee/audio.mp3" type="audio/mpeg" />
            etree.SubElement(pres, 'enclosure', url=mediaRoot + 'audio.mp3', type='audio/mpeg')

            #<guid>http://prod.lcs.uwa.edu.au:8080/ess/echo/presentation/ff6ed436-a3d3-4023-9ff0-86bde10de2ee</guid>
            etree.SubElement(pres, 'guid').text = mediaRoot
            #<media:content url="http://echo360-test.its.uwa.edu.au/echocontent/1339/4/ff6ed436-a3d3-4023-9ff0-86bde10de2ee/audio.mp3" type="audio/mpeg" medium="audio" expression="full" duration="2940" /> 
            #print '    duration (mS) = ' + item['durationMS']
            etree.SubElement(pres, MEDIA + 'content', url=mediaRoot + 'audio.mp3', type='audio/mpeg', medium='audio', expression='full', duration='%d' %(int(item['durationMS'])/1000))

        with open('/var/www/' + captureSummary['section']['course']['identifier'] + 'audio.xml', 'w') as podcastXML:
            podcastXML.write( etree.tostring(root, pretty_print=True, encoding='UTF-8', xml_declaration=True) )
        print captureSummary['section']['course']['identifier'] + 'podcast complete'

exit(0)


